#include "communication.h"
#include <string.h>
#include "BluetoothSerial.h"
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif
#define TIMEOFMEASURE 5
#define HIGHMAX 1.73
BluetoothSerial SerialBT;
char config[16][71] = {"{0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;1;100}",
                       "{0.2660;0.2650;0.2660;0.2660;0.2660;0.2650;0.2700;0.2660;0.2660;1;100}",
                       "{0.2660;0.2652;0.2660;0.2660;0.2660;0.2650;0.2710;0.2660;0.2660;1;100}",
                       "{0.2660;0.2652;0.2660;0.2660;0.2660;0.2650;0.2660;0.2710;0.2710;1;100}",
                       "{0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;1;100}",
                       "{0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2665;1;100}",
                       "{0.2348;0.2340;0.2340;0.2340;0.2340;0.2340;0.2340;0.2340;0.2340;1;100}",
                       "{0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2670;0.2670;1;100}",
                       "{0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;1;100}",
                       "{0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;1;100}",
                       "{0.2660;0.2660;0.2660;0.2660;0.2660;0.2665;0.2660;0.2660;0.2660;1;100}",
                       "{0.2650;0.2660;0.2660;0.2660;0.2660;0.2665;0.2655;0.2665;0.2660;1;100}",
                       "{0.2650;0.2660;0.2655;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;1;100}",
                       "{0.2652;0.2660;0.2650;0.2650;0.2650;0.2660;0.2660;0.2660;0.2660;1;100}",
                       "{0.2650;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;0.2660;1;100}",
                       "{0.2650;0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;0.2665;1;100}"};
int matrix[NUMSLAVE][9];
int matrixLacth[NUMSLAVE][9];
int *ptrMatrixLacth=&matrixLacth[0][0];
int *ptrMatrix=&matrix[0][0];
uint64_t Volume = 0;
uint64_t H = 0;
int count = 0;
// char message[19] = " \r\n hello world \r\n";
void printToSerial(int *arr, int m, int n);
void printToBT(int *arr, int m, int n);
void fillter(int *arr, int m, int n);
void resetArr(int *arr, int m, int n);
void setup()
{
  Serial.begin(115200);
  SerialBT.begin("VTP_TrackVolume_Device");
  init_rs485();
  pinMode(18, OUTPUT);
  digitalWrite(18, LOW); // led status
  configStm(&config[0][0]);
  Serial.println("End config waiting...");
}
void loop()
{ 
  for(int t=0;t<TIMEOFMEASURE;t++)
  {
  getAll(&matrix[0][0], 8000);
  fillter(&matrix[0][0], NUMSLAVE, 9);
  printToSerial(&matrix[0][0], NUMSLAVE, 9);
    for (int i = 0; i < NUMSLAVE; i++)
  {
    for (int j = 0; j < 9; j++)
    {
      if (*(ptrMatrix+i * 9 + j) > *(ptrMatrixLacth+i * 9 + j))
      {
      *(ptrMatrixLacth+i * 9 + j)=*(ptrMatrix+i * 9 + j);
      }
    }
  }
  printToBT(&matrixLacth[0][0], NUMSLAVE, 9);
  }
  Serial.println("Latch data are================== :");
  printToSerial(&matrixLacth[0][0], NUMSLAVE, 9);
   Serial.println("======== END========== :");
  resetArr(&matrixLacth[0][0], NUMSLAVE, 9); 
}
void printToSerial(int *arr, int m, int n)
{
  Serial.print("matrix sensors count:");
  Serial.println(count++);
  for (int i = 0; i < m; i++)
  {
    Serial.print("slave ");
    Serial.print(i + 1);
    Serial.print(":\t");
    for (int j = 0; j < n; j++)
    {
      Serial.print(arr[i * 9 + j]);
      Serial.print("\t");
      H += arr[i * 9 + j];
    }
    Serial.println("");
  }
  Volume = float(H) * 200 * 200;
  Serial.print(" Volume : ");
  Serial.println(Volume);
  H = 0;
}
void printToBT(int *arr, int m, int n)
{
  int sum = 0;
  float Vmax =float(NUMSLAVE)*1.73f*(9.f)* 0.2 * 0.2;; // the tich max
  float T = 0;            // phan tram the tich
  float V;                // the tich con lai
  if (SerialBT.available())
  {
    SerialBT.print("{ ss1 ss2 ss3 ss4 ss5 ss6 ss7 ss8 ss9 ");
    for (int i = 0; i < m; i++)
    {
      SerialBT.print("sl");
      SerialBT.print(i + 1);
      SerialBT.print(" ");
      for (int j = 0; j < n; j++)
      {
        SerialBT.print(arr[i * 9 + j]);
        SerialBT.print(" ");
        sum += arr[i * 9 + j];
      }
    }
    SerialBT.print("/a");
    V = ((float)sum / 1000.f) * 0.2 * 0.2;
    SerialBT.print(V);
    SerialBT.print("/c");
    T = Vmax - V;
    SerialBT.print(T);
    SerialBT.print("]");
  }
}
void fillter(int *arr, int m, int n)
{
  for (int i = 0; i < m; i++)
  {
    for (int j = 0; j < n; j++)
    {
      if (arr[i * 9 + j] == 0)
      {
        arr[i * 9 + j] = (arr[(i - 1) * 9 + j] + arr[(i - 1) * 9 + (j - 1)] + arr[(i - 1) * 9 + (j + 1)] + arr[i * 9 + (j - 1)] + arr[i * 9 + (j + 1)] + arr[(i + 1) * 9 + j] + arr[(i + 1) * 9 + (j - 1)] + arr[(i + 1) * 9 + (j + 1)]) / 8;
      }
    }
  }
}
void resetArr(int *arr, int m, int n)
{
  for (int i = 0; i < m; i++)
  {
    for (int j = 0; j < n; j++)
    {
        arr[i * 9 + j] =0; 
    }
  }
}
